companies = ['Chase', 'Coca Cola', 'Kraft', 'Deloitte', 'Nestle', 'Ford', 'GM', "Paulos' Pastries",  'Starbucks', 'Target', 'Walmart']
queueSize = [45, 32, 23, 2, 7, 10, 15, 4, 9, 12, 27]

# Set background
bg = new BackgroundLayer backgroundColor: "#C5EFF7"
bg.bringToFront()

headerLayer = new Layer
	x: 0, y: 0
	width: 640,
	height: 120,
	backgroundColor: "#19B5FE"
	
uploadResumeButton = new Layer
	x: 540
	y: 30
	image: "images/resumeupload.png"
	width: 65
	height: 65

titleLayer = new Layer
	width: 640
	height: 70
	y: 20
	backgroundColor: ""
titleLayer.html = "<center><h1><br/>Q"
titleLayer.bringToFront()
titleLayer.superLayer = headerLayer
lineLayer = new Layer
	width: 640
	height: 2
	y: 118
	backgroundColor: "rgba(0, 0, 0, 0.4)"
lineLayer.superLayer = headerLayer

# Create ScrollComponent
scroll = new ScrollComponent
	width: 640
	height: 1136-120,
	y: 120,
	scrollHorizontal: false
	backgroundColor: "rgba(255,255,255,0.0)"
	contentInset:
		top: 10
		bottom: 10

maxQueue = 45

# Create 10 layers
for i in [0..10]
	layerA = new Layer
		width: 620, height: 120
		x: 10, y: 140 * i
		backgroundColor: "#fff"
		superLayer: scroll.content
		borderRadius: 4
		
	titleText = new Layer
		width: 570, height: 40
		backgroundColor: ""
		superLayer: layerA
	titleText.html = "<font color='black' style='font-size: 120%'>"+companies[i]
	titleText.center()
	titleText.y = 45
	
	w = queueSize[i]*3
	
	queueSizeText = new Layer
		width: w
		height: 45
		x: 450+(maxQueue*3 - w)
		y: 40
		backgroundColor: "#D91E18"
		superLayer: layerA
		
	if queueSize[i] < 25
		queueSizeText.backgroundColor = "#F7CA18"
	if queueSize[i] < 10
		queueSizeText.backgroundColor = "#2ECC71"
		
	if i == 10
		layerA.on Events.Click, ->
			scroll.animate
				properties:
					x: -640
			companyScroll.animate
				properties:
					x: 0
			titleLayer.animate
				properties:
					x: -640
			companyTitleLayer.animate
				properties:
					x: 0
			uploadResumeButton.animate
				properties:
					x: -100

companyScroll = new ScrollComponent
	width: 640
	height: 1136-120,
	y: 120,
	x: 640
	scrollHorizontal: false
	backgroundColor: "rgba(255,255,255,0.0)"
	contentInset:
		top: 10
		bottom: 10
		
info = new Layer
	superLayer: companyScroll.content
	width: 620
	height: 230
	x: 10
	backgroundColor: "#FFF"
	borderRadius: 4
		
amazonPic = new Layer
	superLayer: info
	width: 500
	height: 131
	backgroundColor: "#FFF"
	borderRadius: 4
	image: 'images/walmart.jpg'
amazonPic.center()
amazonPic.y = 55

amazonInfo = new Layer
	superLayer: companyScroll.content
	width: 620
	height: 120
	y: 245
	x: 10
	backgroundColor: "#FFF"
	borderRadius: 4
	contentInset:
		top: 10
		bottom: 10
		left: 10


textLayer = new Layer
	superLayer: amazonInfo
	width: 600
	height: amazonInfo.height - 20
	x: 10
	y: 15
	backgroundColor: ""
textLayer.html = "<font color='black'>Walmart  is an American multinational retail corporation that operates a chain of discount department stores and warehouse stores."

queueLayer = new Layer
	superLayer: companyScroll.content
	width: 620
	height: 100
	y: 380
	x: 10
	borderRadius: 4
	backgroundColor: "#2ECC71"
	borderColor: "#FFF"
	borderWidth: 3
	
queueLayer.html="<h2><center><br>Join Queue! (45)"

queueLayer.on Events.Click, ->
	queueLayer.html="<h2><center><br>Added!"

companyTitleLayer = new Layer
	width: 640
	height: 70
	y: 20
	x: 640
	backgroundColor: ""
companyTitleLayer.html = "<center><h1><br/>Walmart"
companyTitleLayer.bringToFront()
companyTitleLayer.superLayer = headerLayer

resumeUploadScreen = new Layer
	width: 640
	height: 1136
	backgroundColor: "#FFF"
	y: 1136
	
	
resumeHeaderLayer = new Layer
	x: 0, y: 0
	width: 640,
	height: 120,
	backgroundColor: "#1F3A93"
	superLayer: resumeUploadScreen
	
resumeTitleLayer = new Layer
	width: 640
	height: 80
	y: 20
	backgroundColor: ""
	superLayer: resumeHeaderLayer
resumeTitleLayer.html = "<center><h1><br/>Google Drive"

resumeObject = new Layer
	width: 270
	height: 340
	borderRadius: 4
	backgroundColor: "#E4F1FE"
	borderWidth: 3
	borderColor: "#000"
	x: 30
	y: 160
	superLayer: resumeUploadScreen
resumeObject.html = "<font color='black'><br/><br/><br/><h3><center>resume.pdf"

uploadResumeButton.on Events.Click, ->
	resumeUploadScreen.animate
		properties:
			y: 0

resumeObject.on Events.Click, ->
	resumeUploadScreen.animate
		properties:
			y: 1136
