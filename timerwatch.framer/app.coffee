
companies = ["Apple", "Amazon", "IDEO", "Walmart"]
queue = ["2", "46", "50", "51"]
options = ["Walmart"]
singleClick = false

# Set background
bg3 = new Layer
	width: 320
	height: 320
	borderRadius: 160
	backgroundColor: "#000"
bg3.center()
bg2 = new Layer
	width: 320
	height: 320
	borderRadius: 160
	backgroundColor: "#2ECC71"
	opacity: 0
bg2.center()
bg = new Layer
	width: 320
	height: 320
	borderRadius: 160
	backgroundColor: ""
bg.center()

# Center ScrollComponent
# scroll.center()

# optionsScroll.x = 272

timerLayer = new Layer
		width: 180
		height: 60
		superLayer: bg
		x: 70
		y: 100
		backgroundColor: ""
		opacity: 1
timerLayer.html="<center><font color='red'>60"



layerA = new Layer
	width: 230, height: 230
	x: 45
	y: 45
	backgroundColor: "#fff"
	superLayer: bg
	borderRadius: 125
layerA.bringToFront()
	
textLayer = new Layer
	width: 180
	height: 60
	superLayer: layerA
	x: 25
	y: 40
	backgroundColor: ""
textLayer.html = "<center><h3><font color='#000000'>"+options[0]
	
timerLayer.bringToFront()
	
    	
timerCall = (time) ->
	timerLayer.html="<center><br><font color='red' size='200%'>"+time+"s"
	setTimeout ( ->
	  timerCall time-1
	), 1000

timerCall(60)

accept = new Layer
	backgroundColor: "#2ECC71"
	width: 60
	height: 60
	borderRadius: 30
	superLayer: bg
	x: 170
	y: 180
	
accept.html = " <center><div style='padding-top: 18px'><h2>✓"
accept.bringToFront()

	
decline = new Layer
	backgroundColor: "#CF000F"
	width: 60
	height: 60
	borderRadius: 30
	superLayer: bg
	x: 90
	y: 180
decline.html = "<center><div style='padding-top: 14px'><h2>x"


accept.on "click", ->
	accept.animate
		properties: 
			opacity: 0
	decline.animate
		properties: 
			opacity: 0
	bg2.animate
		properties: 
			opacity: 1
	textLayer.animate
		properties: 
			y: 70
	timerLayer.animate
		properties: 
			y: 150
	

# ADD SKIN ON TOP OF EVERYTHING ELSE
moto360 = new Layer
	width: 600, height: 900
	image: "images/moto_moto360-mask.png"
moto360.scaleX = 0.60
moto360.scaleY = 0.60
moto360.center()
	