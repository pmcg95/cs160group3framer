
companies = ["Apple", "Amazon", "IDEO", "Walmart"]
queue = ["2", "46", "50", "51"]
options = ["John Doe", "Derek Chiu"]
singleClick = false

# Set background
bg = new Layer
	width: 320
	height: 320
	borderRadius: 160
bg.center()
bg2 = new Layer
	width: 320
	height: 320
	borderRadius: 160
	backgroundColor: "#000"
	opacity: 0
bg2.center()

# Center ScrollComponent
# scroll.center()

optionsScroll = new PageComponent
	width: 320
	height: 320
	scrollVertical: false
	scrollHorizontal: true
	backgroundColor: ""
	contentInset: 
		top: 0
		right: 45
optionsScroll.center()
# optionsScroll.x = 272

timerLayer = new Layer
		width: 180
		height: 60
		superLayer: optionsScroll.content
		x: 320+70
		y: 180
		backgroundColor: ""
		opacity: 0
timerLayer.html="<center><font color='red'>45s"


scroll.superLayer = optionsScroll.content

for i in [0..1]
	layerA = new Layer
		width: 230, height: 230
		x: 320*(i) + 45
		y: 45
		backgroundColor: "#fff"
		superLayer: optionsScroll.content
		borderRadius: 125
		
	textLayer = new Layer
		width: 180
		height: 60
		superLayer: layerA
		x: 25
		y: 100
		backgroundColor: ""
		
	layerA.on "click", ->
	    bg2.animate
	        properties:
	            opacity: 0.0
	    timerLayer.animate
	    	properties: 
	    		opacity: 0.0
		
	textLayer.html = "<center><h3><font color='#000000'>"+options[i]
	
timerLayer.bringToFront()
		
optionsScroll.on "change:currentPage", ->
    bg2.animate
        properties:
            opacity: 0.9
    timerLayer.animate
    	properties: 
    		opacity: 1
    timerCall(60)
    		
timerCall = (time) ->
	timerLayer.html="<center><font color='red'>"+time+"s"
	setTimeout ( ->
	  timerCall time-1
	), 1000


# ADD SKIN ON TOP OF EVERYTHING ELSE
moto360 = new Layer
	width: 600, height: 900
	image: "images/moto_moto360-mask.png"
moto360.scaleX = 0.60
moto360.scaleY = 0.60
moto360.center()
	