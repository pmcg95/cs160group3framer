students = ['John Doe', 'Patrick Mc Gartoll', 'Tomo Ueda', 'Daniel He', 'Derek Chiu', 'Roy Kim', 'John Smith', 'Ronald McDonald']

# Set background
bg = new BackgroundLayer backgroundColor: "#86E2D5"
bg.bringToFront()

headerLayer = new Layer
	x: 0, y: 0
	width: 640,
	height: 120,
	backgroundColor: "#2ECC71"
	
uploadResumeButton = new Layer
	x: 540
	y: 30
	image: "images/resumeupload.png"
	width: 65
	height: 65

titleLayer = new Layer
	width: 640
	height: 70
	y: 20
	backgroundColor: ""
titleLayer.html = "<center><h1><br/>QRecruiter"
titleLayer.bringToFront()
titleLayer.superLayer = headerLayer
lineLayer = new Layer
	width: 640
	height: 2
	y: 118
	backgroundColor: "rgba(0, 0, 0, 0.4)"
lineLayer.superLayer = headerLayer

waitingLayer = new Layer
	width: 640
	height: 40
	y: 140
	backgroundColor: ""
waitingLayer.html = "<center><font color='black'>Queue Size: 8"

# Create ScrollComponent
scroll = new ScrollComponent
	width: 640
	height: 1136-180,
	y: 180,
	scrollHorizontal: false
	backgroundColor: "rgba(255,255,255,0.0)"
	contentInset:
		top: 10
		bottom: 10

maxQueue = 45

# Create 10 layers
for i in [0..7]
	layerA = new Layer
		width: 620, height: 120
		x: 10, y: 140 * i
		backgroundColor: "#fff"
		superLayer: scroll.content
		borderRadius: 4
		
	titleText = new Layer
		width: 570, height: 40
		backgroundColor: ""
		superLayer: layerA
	titleText.html = "<font color='black' style='font-size: 120%'>"+students[i]
	titleText.center()
	titleText.y = 45
	
	if i == 0
		layerA.on Events.Click, ->
			scroll.animate
				properties:
					x: -640
			companyScroll.animate
				properties:
					x: 0
			waitingLayer.animate
				properties: 
					x: -640
			titleLayer.animate
				properties:
					x: -640
			companyTitleLayer.animate
				properties:
					x: 0
			uploadResumeButton.animate
				properties:
					x: -100

companyScroll = new ScrollComponent
	width: 640
	height: 1136-120,
	y: 120,
	x: 640
	scrollHorizontal: false
	backgroundColor: "rgba(255,255,255,0.0)"
	contentInset:
		top: 10
		bottom: 10
		
info = new Layer
	superLayer: companyScroll.content
	width: 620
	height: 800
	x: 10
	backgroundColor: "#FFF"
	borderRadius: 4
	image: "images/resume.jpg"
		



companyTitleLayer = new Layer
	width: 640
	height: 70
	y: 20
	x: 640
	backgroundColor: ""
companyTitleLayer.html = "<center><h1><br/>John Doe"
companyTitleLayer.bringToFront()
companyTitleLayer.superLayer = headerLayer

