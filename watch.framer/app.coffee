
companies = ["Coca-Cola", "Walmart", "Chase", "Ford"]
queue = ["2", "46", "50", "51"]
options = ["Drop out of queue", "Bump back</br>in queue"]
images = ["images/user-exit-512.png", "https://image.freepik.com/free-icon/back-arrow--ios-7-interface-symbol_318-33678.jpg"]
singleClick = false

# Set background
bg = new Layer
	width: 320
	height: 320
	borderRadius: 160
bg.center()

# Create ScrollComponent
scroll = new PageComponent
	width: 320
	height: 320
	scrollHorizontal: false
	scrollVertical: true
	backgroundColor: ""
	contentInset:
		bottom: 35
		top: 0
		left: 2
	borderRadius: 0

# Center ScrollComponent
# scroll.center()

optionsScroll = new PageComponent
	width: 320
	height: 320
	scrollVertical: false
	scrollHorizontal: true
	backgroundColor: ""
	contentInset: 
		top: 0
		right: 40
optionsScroll.center()
# optionsScroll.x = 272

scroll.superLayer = optionsScroll.content

for i in [0..1]
	layerA = new Layer
		width: 280, height: 280
		x: 320*(i+1)+20
		y: 20
		backgroundColor: ""
		superLayer: optionsScroll.content
		borderRadius: 140
		
	layerC = new Layer
		width: 110, height: 110
		x: 85
		y: 85
		backgroundColor: "#fff"
		superLayer: layerA
		borderRadius: 125
		
	layerB = new Layer
		width: 60, height: 60
		x: 110
		y: 110
		backgroundColor: "#fff"
		superLayer: layerA
		borderRadius: 0
		
	textLayer = new Layer
		width: 200, height: 60
		x: 40
		y: 210
		backgroundColor: ""
		superLayer: layerA
		
	layerB.image = images[i]
		
	textLayer.html = "<center><font color='#fff'><h5>"+options[i]
		


for i in [0..3]
	layerA = new Layer
		width: 250, height: 250
		x: 35, y: (320 * i) + 35
		backgroundColor: "#fff"
		superLayer: scroll.content
		borderRadius: 125
		
	queuePositionText = new Layer
		backgroundColor: ""
		width: 250
		height: 100
		
	queuePositionText.html ="<b><br/><font color='#D91E18' style='font-size: 250%'>"+queue[i]+"</font>"
	layerA.addSubLayer(queuePositionText)
	queuePositionText.center()
	queuePositionText.x = 120
	queuePositionText.y = 130
	
	queueImage = new Layer
		width: 34
		height: 60
		image: "http://images.clipartpanda.com/google-location-icon-location.png"
		superLayer: layerA
		x: 80
		y: 140
		
	
	companyNameText = new Layer
		backgroundColor: ""
		width: 250
		height: 100
		
	companyNameText.html ="<center><br/><font color='#333' style='font-size: 150%'>"+companies[i]+"</font>"
	layerA.addSubLayer(companyNameText)
	companyNameText.center()
	companyNameText.y = 40
	
# ADD SKIN ON TOP OF EVERYTHING ELSE
moto360 = new Layer
	width: 600, height: 900
	image: "images/moto_moto360-mask.png"
moto360.scaleX = 0.60
moto360.scaleY = 0.60
moto360.center()
	